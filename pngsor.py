from PIL import Image
im = Image.open("alyssa-log.128.png")
(w,h) = im.size
# each pixel to vertices
vertices = []
for y in range(0,h):
    for x in range(0,w):
        p = im.getpixel((x,y))
        v = p[0]/p[1]
        vertices.append((x,y,v))
faces = []
for y in range(0,h):
    for x in range(0,w):
        face = [y * w + x,
                y * w + (x+1)%w,
                ((y+1)%w) * w + (x+1)%w,
                ((y+1)%w) * w + x%w
	]
        faces.append(face)
