import math
from PIL import Image

def rotate(x,y,rot):
   xr = x * math.cos(rot) - y * math.sin(rot)
   yr = y * math.cos(rot) + x * math.sin(rot)
   return (xr,yr)

im = Image.open("alyssa-log.128.png")
(w,h) = im.size
# each pixel to vertices

vertices = []
baseradius = 1.0
offset = 1.0
# y is constant?
for y in range(0,h):
    for x in range(0,w):
        p = im.getpixel((x,y))
        v = offset*p[0]/p[1]
        (xx,zz) = rotate(baseradius + v,0.0,2*math.pi*x/w)
        vertices.append((xx,y,zz))

faces = []
for y in range(0,h-1):
    for x in range(0,w):
        face = [y * w + x,
                y * w + (x+1)%w,
                ((y+1)%w) * w + (x+1)%w,
                ((y+1)%w) * w + x%w
	]
        faces.append(face)

# caps
faces.append([(h-1)*w + x for x in range(0,w)]
faces.append([0*w + x for x in range(0,w)]
