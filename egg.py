import bpy
import mathutils
import math

"""
filename="/home/hindle1/projects/blender/egg.py"
exec(compile(open(filename).read(), filename, 'exec'))
"""

def rotate(x,y,rot):
   xr = x * math.cos(rot) - y * math.sin(rot)
   yr = y * math.cos(rot) + x * math.sin(rot)
   return (xr,yr)

vertices = []

minx = -1.0
maxx = 1.0


# x²/9+y²/4*t2(x)=1
# y²/4*t2(x)=1 - x²/9
# y²/4 = (1 - x²/9)/t2(x)
# y² = 4*(1 - x²/9)/t2(x)
# y = sqrt(4*(1 - x²/9)/t2(x))
# y = sqrt(4*(1 - x*x/9)/t2(x))
# 
# t2(x) = 1/1-0.2*x
def t2(x):
    return 1/(1-0.2*x)
def ellipsey(x):
    return sqrt(4*(1 - x*x/9)/t2(x))


class MakeEGG(bpy.types.Operator):
    bl_idname = "mesh.make_egg"
    bl_label = "Add egg"
    segs = 256
    minx = -3.0
    maxx =  3.0

    def invoke(self, context, event):
        (vertices, edges) = self.getVerticesAndEdges()
        Vertices = [mathutils.Vector(x) for x in vertices]
        NewMesh = bpy.data.meshes.new("EGG")
        NewMesh.from_pydata(Vertices,edges,[])
        NewMesh.update()
        NewObj = bpy.data.objects.new("EGG", NewMesh)
        context.scene.objects.link(NewObj)
        return {"FINISHED"}

    def seg2x(self,x):
        return (self.maxx - self.minx)*x/self.segs + self.minx

    def getVerticesAndEdges(self):
        vertices = [(self.seg2x(x),ellipsey(self.seg2x(x)),0) for x in range(0,self.segs+1)]
        edges = [[i,i+1] for i in range(0,len(vertices)-1)]
        return (vertices, edges)
    
bpy.utils.register_class(MakeEGG)
