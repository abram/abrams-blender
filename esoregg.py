import bpy
import mathutils
import math
from PIL import Image

"""
filename="/home/hindle1/projects/blender/esoregg.py"
exec(compile(open(filename).read(), filename, 'exec'))
"""

def rotate(x,y,rot):
   xr = x * math.cos(rot) - y * math.sin(rot)
   yr = y * math.cos(rot) + x * math.sin(rot)
   return (xr,yr)

def t2(x):
    return 1/(1-0.2*x)
def ellipsey(x):
    return sqrt(4*(1 - x*x/9)/t2(x))

class MakeSOREGG(bpy.types.Operator):
    bl_idname = "mesh.make_sor_egg"
    bl_label = "Add soregg"
    baseradius = 1.5
    offset = 1.0
    segs = 256
    minx = -3.0
    maxx =  3.0

    def invoke(self, context, event):
        (vertices, faces) = self.getVerticesAndFaces()
        Vertices = [mathutils.Vector(x) for x in vertices]
        NewMesh = bpy.data.meshes.new("SOREGG")
        NewMesh.from_pydata(Vertices,[],faces)
        NewMesh.update()
        NewObj = bpy.data.objects.new("SOREGG", NewMesh)
        context.scene.objects.link(NewObj)
        return {"FINISHED"}

    def seg2x(self,x):
        return (self.maxx - self.minx)*x/self.segs + self.minx

    def getVerticesAndFaces(self):
        im = Image.open("/home/hindle1/projects/blender/neil-martin-hindle.256.png")
        (w,h) = im.size
        # each pixel to vertices
        vertices = []
        for y in range(0,h):
            for x in range(0,w):
                p = im.getpixel((x,y))
                v = self.offset*p[0]/p[1]
                #(xx,zz) = rotate(self.baseradius + v,0.0,2*math.pi*x/w)
                (xx,zz) = rotate(ellipsey(self.seg2x(y))/2 + self.baseradius*v,0.0,2*math.pi*x/w)
                vertices.append((xx,self.seg2x(y),zz))

        faces = []
        for y in range(0,h-1):
            for x in range(0,w):
                face = [y * w + x,
                        y * w + (x+1)%w,
                        ((y+1)%w) * w + (x+1)%w,
                        ((y+1)%w) * w + x%w
                       ]
                faces.append(face)
        # caps
        faces.append([(h-1)*w + x for x in range(0,w)])
        faces.append([0*w + x for x in range(0,w)])   
        return (vertices, faces)
    
bpy.utils.register_class(MakeSOREGG)


